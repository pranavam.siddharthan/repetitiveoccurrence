package org.xigma.logics;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class RepetitiveOccurrence {

	private Map<String, Integer> repeatedCollection = new HashMap<>();

	public String extractRepetitionForValueAbove(final int count) {
		return this.repeatedCollection.keySet()
				.stream()
				.filter(x -> getElementValue(x) > 1)
				.collect(Collectors.joining())
				.toString();
	}

	public void addFunctional(String... values) {
		Arrays.asList(values)
				.stream()
				.forEach(x -> this.repeatedCollection.put(x, getElementValue(x) + 1));
	}

	private Integer getElementValue(String x) {
		if (this.repeatedCollection.containsKey(x))
			return this.repeatedCollection.get(x);
		else
			return 0;
	}

}
