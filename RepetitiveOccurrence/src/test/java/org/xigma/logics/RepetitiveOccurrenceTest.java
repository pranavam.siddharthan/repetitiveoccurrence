package org.xigma.logics;

import static org.junit.Assert.*;

import org.junit.Test;

public class RepetitiveOccurrenceTest {

	@Test
	public void givenCollectionWithAllValuesAsOne_whenRequestedForKeys_thenReturnEmpty() {
		RepetitiveOccurrence repetitiveOccurrence = new RepetitiveOccurrence();
		repetitiveOccurrence.addFunctional("A", "B", "C");
		String actualValue = repetitiveOccurrence.extractRepetitionForValueAbove(1);
		assertEquals("", actualValue);
	}

	@Test
	public void givenCollectionWithAValuesAsTwo_whenRequestedForKeys_thenReturnA() {
		RepetitiveOccurrence repetitiveOccurrence = new RepetitiveOccurrence();
		repetitiveOccurrence.addFunctional("A", "B", "C", "A");
		repetitiveOccurrence.addFunctional("A", "B", "A");
		String actualValue = repetitiveOccurrence.extractRepetitionForValueAbove(1);
		assertEquals("AB", actualValue);
	}
}
