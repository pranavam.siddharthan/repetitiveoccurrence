# Identify Repetitive Occurrence

### A simple code to find the repetitive occurrence in a stream of characters.

---
##### Example:

###### In a stream of [A|B|C|E|D|A|E|F|A|] we can see that A, E repeats.  So this piece of code would return "AE" for the given stream.

---

Written under the standards of Java SE 8 utilizing functional programming.


